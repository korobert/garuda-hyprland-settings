#!/bin/sh

# Check if the system is running on battery
if upower -e | grep -q "battery"; then
    # System is running on battery
    LAPTOP=true
else
    # System is not running on battery, likely a desktop or on AC power
    LAPTOP=false
fi

# Path to the power supply attribute file
POWER_SUPPLY_FILE="/sys/class/power_supply/AC/online"

# Read the value of the online attribute
online=$(cat "$POWER_SUPPLY_FILE")

# Check if the system is on AC power (charging)
if [ "$online" -eq 1 ]; then
    # System is on AC power (charging)
    AC_POWER=true
else
    # System is not on AC power (charging)
    AC_POWER=false
fi

# Run commands based on system type and power source
if $LAPTOP; then
    # Run commands for laptops
    if $AC_POWER; then
        # Run commands for laptops on AC power
        swayidle -w \
            timeout 60 'temp=$(brightnessctl g); brightnessctl set $((temp / 3))' \
            resume 'temp=$(brightnessctl g); brightnessctl set $((temp * 3))' \
            timeout 90 'swaylock -f & sleep 1' \
            timeout 300 'hyprctl dispatch dpms off' \
            resume 'hyprctl dispatch dpms on' \
            timeout 600 'systemctl suspend'
       
    else
        # Run commands for laptops on battery
         swayidle -w \
            timeout 60 'temp=$(brightnessctl g); brightnessctl set $((temp / 3))' \
            resume 'temp=$(brightnessctl g); brightnessctl set $((temp * 3))' \
            timeout 90 'swaylock -f & sleep 1' \
            timeout 180 'hyprctl dispatch dpms off' \
            resume 'hyprctl dispatch dpms on' \
            timeout 420 'systemctl suspend'
    fi
else
    # Run commands for desktops
    swayidle -w \
        timeout 90 'swaylock -f & sleep 1' \
        timeout 300 'hyprctl dispatch dpms off' \
        resume 'hyprctl dispatch dpms on' \
        timeout 600 'systemctl suspend'
fi
